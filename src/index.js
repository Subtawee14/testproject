import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'antd/dist/antd.css';
import { createStore, applyMiddleware, compose } from "redux";
import { rootReducer } from "./reducer/index";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import MainRouting from './Route'

const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?   
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;


const enhancer = composeEnhancers(
  applyMiddleware(thunk),
  // other store enhancers if any
);
const appStore = createStore(rootReducer, enhancer);


ReactDOM.render(
  <Provider store={appStore}>
    <MainRouting />
  </Provider>,

  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
