
export const FETCH_DATA_BEGIN = "FETCH_DATA_BEGIN";
export const FETCH_DATA_SUCCESS = "FETCH_DATA_SUCCESS";
export const FETCH_DATA_ERROR = "ERROR";


export const SEARCH_DATA = "SEARCH_DATA";


export const fetchDataBegin = () => {
  return {
    type: FETCH_DATA_BEGIN
  };
};

export const fetchDataSuccess = (data) => {
  return {
    type: FETCH_DATA_SUCCESS,
    payLoad : data
  };
};

export const fetchDataError = (error) => {
  return {
    type: FETCH_DATA_ERROR,
    payLoad : error
  };
};

export const searchFilter = (text) => {
  return {
    type: SEARCH_DATA,
    payLoad : text
  };
};



