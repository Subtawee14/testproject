
export const SELECTED_MENU = "SELECTED_MENU";

export const SelectedMenu = (key) => {
  return {
    type: SELECTED_MENU,
    payLoad : key
  };
};