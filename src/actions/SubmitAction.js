
export const SUBMIT = "SUBMIT";

export const SubmitAction = (data) => {
  return {
    type: SUBMIT,
    payLoad : data
  };
};