import { combineReducers } from "redux";
import { fetchReducer } from "./fetchReducer";
import { MenuReducer } from './MenuReducer'

export const rootReducer = combineReducers({
  data: fetchReducer,
  menu : MenuReducer
});
