import {
  FETCH_DATA_BEGIN,
  FETCH_DATA_SUCCESS,
  FETCH_DATA_ERROR,
  fetchDataBegin,
  fetchDataSuccess,
  fetchDataError,
  SEARCH_DATA
} from "../actions/FetchAction";
import { data } from "../data";
import axios from "axios";
import { detail } from "../data";
import { SUBMIT } from "../actions/SubmitAction";

export const fetchData = () => {
  return dispatch => {
    dispatch(fetchDataBegin());

    setTimeout(() => {
      dispatch(fetchDataSuccess(detail));
    }, 1000);
  };
};

const initialState = {
  data: [],
  loading: false,
  err: "",
  searchText: ""
};

export const fetchReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DATA_BEGIN:
      return {
        ...state,
        loading: true
      };
    case FETCH_DATA_SUCCESS:
      return {
        data: action.payLoad,
        loading: false
      };
    case FETCH_DATA_ERROR:
      return {
        ...state,
        loading: false,
        err: action.payLoad
      };
    case SEARCH_DATA:
      return {
        ...state,
        searchText: action.payLoad
      };
    case SUBMIT:
      let newData = action.payLoad;
      console.log("newData", newData);

      let newArray = [...state.data];
      console.log("newArray", newArray);

      let inex = 0;
      newArray.forEach((value, index) => {
        if (value.id == newData.id) {
          console.log("value", value);
          inex = index;
          console.log("inex", inex);
          return;
        }
      });

      newArray[inex] = newData;
      // alert("Submit");
      return {
        ...state,
        data: newArray
      };

    default:
      return state;
  }
};
