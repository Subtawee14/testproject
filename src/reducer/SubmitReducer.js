import { SUBMIT } from "../actions/SubmitAction";

const initialObj = {
  name: "",
  required_age: 0
};

export const SubmitReducer = (state = initialObj, action) => {
  switch (action.type) {
    case SUBMIT:
        
      return {
        name: action.payLoad.name,
        required_age: action.payLoad.required_age
      };
    default:
      return state;
  }
};
