import React from 'react';
import Main from './components/Main';
import { BrowserRouter } from "react-router-dom";
const MainRouting = () => {
    return (
      <BrowserRouter>
        <Main/>
      </BrowserRouter>
    );
  };
  
  export default MainRouting;
  