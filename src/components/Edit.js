import React, { useState, useEffect } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { SubmitAction } from "../actions/SubmitAction";
import { fetchData } from "../reducer/fetchReducer";
import { useHistory } from "react-router-dom";
import {
  Form,
  Input,
  Tooltip,
  Icon,
  Cascader,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  AutoComplete
} from "antd";
const { TextArea } = Input;
const Edit = props => {
  const [name, setName] = useState();
  const [age, setAge] = useState();
  const [type, setType] = useState();
  const [detail, setDetail] = useState();
  const [about, setAbout] = useState();
  const [short_detail, setShort_detail] = useState();
  const [EditingData, setEditingData] = useState();
  const { fetchData } = props;
  const { SubmitAction, id } = props;
  const { data } = props.data;
  let history = useHistory();
  useEffect(() => {
    if (data.length == 0) {
      fetchData();
    }
  }, []);

  useEffect(() => {
    fetchDataFilter();
  }, [data]);

  useEffect(() => {
    if (EditingData) {
      setName(EditingData.data.name);
      setAge(EditingData.data.required_age);
      setType(EditingData.data.type);
      setDetail(EditingData.data.detailed_description);
      setAbout(EditingData.data.about_the_game);
      setShort_detail(EditingData.data.short_description);
    }
  }, [EditingData]);

  const fetchDataFilter = () => {
    const filter = data.find(value => {
      return value.id == props.match.params.id;
    });
    setEditingData(filter);
  };

  const handleSubmit = event => {
    event.preventDefault();
    const data = {
      id: props.match.params.id,
      success: true,
      data: {
        type: type,
        name: name,
        steam_appid: props.match.params.id,
        required_age: age,
        detailed_description: detail,
        about_the_game: about,
        short_description: short_detail,
        header_image: EditingData.data.header_image,
        website: EditingData.data.website,
        screenshots: EditingData.data.screenshots
      }
    };
    console.log(data);
    SubmitAction(data);
    history.push('/')
  };
  console.log(EditingData ? EditingData.data.name : "");

  const renderForm = () => {
    return (
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col span={7}>
            <Form.Item label="name">
              <Input
                type="text"
                placeholder="name"
                value={name}
                onChange={e => {
                  setName(e.target.value);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={5}>
            <Form.Item label="required_age">
              <Input
                type="number"
                placeholder="required age"
                value={age}
                onChange={e => setAge(e.target.value)}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={3}>
            <Form.Item label="Type">
              <Input
                type="text"
                placeholder="type"
                value={type}
                onChange={e => {
                  setType(e.target.value);
                }}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={7}>
            <Form.Item label="Detail">
              <TextArea
                rows={4}
                type="text"
                placeholder="detail"
                value={detail}
                onChange={e => {
                  setDetail(e.target.value);
                }}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={7}>
            <Form.Item label="About">
              <TextArea
                rows={4}
                type="text"
                placeholder="about"
                value={about}
                onChange={e => {
                  setAbout(e.target.value);
                }}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={7}>
            <Form.Item label="Short Detail">
              <TextArea
                rows={4}
                type="text"
                placeholder="short Detail"
                value={short_detail}
                onChange={e => {
                  setShort_detail(e.target.value);
                }}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Update
            </Button>
          </Form.Item>
        </Row>
      </Form>
    );
  };
  return <React.Fragment>{renderForm()}</React.Fragment>;
};

const mapStateToProps = state => {
  return {
    data: state.data
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ SubmitAction, fetchData }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
