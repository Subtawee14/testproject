import React, { useEffect, useState } from "react";
import axios from "axios";
import { List, Avatar, Icon } from "antd";
import { detail } from "../data";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
const GameItemList = props => {
  const [gameDetail, setGameDetail] = useState();
  const { data } = props.data;
  const { id } = props;
  let history = useHistory();
  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = () => {
    console.log("data", data);
    const filter = data.find(value => {
      return value.id == id;
    });

    setGameDetail(filter.data);
    // axios.get("http://localhost:3000/gamedetails/" + id).then((response) => {
    //   console.log(response.data.data);
    //   setData(response.data.data);
    // });
  };
  console.log(gameDetail);

  const IconText = ({ type, text }) => (
    <span>
      <Icon type={type} style={{ marginRight: 8 }} />
      {text}
    </span>
  );
  const renderItem = () => {
    if (gameDetail) {
      return (
        <List.Item
          key={id}
          actions={[
            <IconText type="star-o" text="156" key="list-vertical-star-o" />,
            <IconText type="like-o" text="156" key="list-vertical-like-o" />,
            <IconText type="message" text="2" key="list-vertical-message" />
          ]}
          extra={
            <img
              width={272}
              alt="logo"
              src={gameDetail.screenshots[0].path_full}
            />
          }
        >
          <List.Item.Meta
            avatar={<Avatar src={gameDetail.screenshots[1].path_thumbnail} />}
            title={
              <a
                onClick={() => {
                  history.push("/detail/" + id);
                }}
              >
                {gameDetail.name}
              </a>
            }
            description={gameDetail.website}
          />
          {gameDetail.short_description}
        </List.Item>
      );
    } else {
      return <div></div>;
    }
  };

  return (
    <div style={{border : "1px solid black",padding : "10px" ,margin :"10px"}}>
      <p>
        ID : {id}
        <a
          onClick={() => {
            history.push("/edit/" +id);
          }}
        >
          <Icon type="edit" style={{ fontSize: "20px", color: "#08c" ,marginLeft : "10px"}} />
        </a>
      </p>
      {renderItem()}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    data: state.data
  };
};
// const mapDispatchToProps = (dispatch) => {
//   return bindActionCreators({ fetchData }, dispatch);
// };

export default connect(mapStateToProps, null)(GameItemList);
