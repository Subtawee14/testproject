import React, { useState } from "react";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import GameList from "./GameList";
import { Route } from "react-router-dom";
import Detail from "./Detail";
import { useHistory } from "react-router-dom";
import { SelectedMenu } from "../actions/MenuAction";
import Edit from "./Edit";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
const Main = props => {
  const [collapsed, setCollapsed] = useState(false);
  let history = useHistory();

  const onCollapse = () => {
    setCollapsed(!collapsed);
  };
  const SelectedMenu = key => {
    if (key == 1) {
      history.push("/");
    } else if (key == 3) {
      history.push("/edit");
    } else {
      history.push("/");
    }
  };
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider collapsible collapsed={collapsed} onCollapse={e => onCollapse()}>
        <div className="logo" />
        <Menu
          theme="dark"
          defaultselectedKeys={[1]}
          mode="inline"
          onSelect={e => SelectedMenu(e.key)}
        >
          <Menu.Item key="1">
            <Icon type="desktop" />
            <span>HOME</span>
          </Menu.Item>
          <Menu.Item key="2">
            <Icon type="pie-chart" />
            <span>DETAIL</span>
          </Menu.Item>
          <Menu.Item key="3">
            <Icon type="edit" />
            <span>Edit</span>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Content style={{ margin: "16px" }}>
          <div
            style={{
              padding: 24,
              background: "#fff",
              minHeight: 360,
              height: "100%"
            }}
          >
            <Route exact path="/" component={GameList} />
            <Route path="/edit/:id" component={Edit} />
            <Route path="/detail/:id" component={Detail} />
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
};

export default Main;
