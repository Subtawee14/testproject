import React, { useState, useEffect } from "react";
import axios from "axios";
import { List, Avatar, Icon } from "antd";
import GameItemList from "./GameItemList";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchData } from "../reducer/fetchReducer";
import { Spin } from "antd";
import { Input } from "antd";
import { searchFilter } from "../actions/FetchAction";
const { Search } = Input;
const GameList = props => {
  const { fetchData, searchFilter } = props;
  const { data, loading, err,searchText } = props.data;
  useEffect(() => {
    if (data.length == 0) {
      fetchData();
    }
  }, []);


  console.log("data", data);
  return (
    <div>
      
      <Spin spinning={loading}>
        <List
          itemLayout="vertical"
          size="large"
          pagination={{
            onChange: page => {
              console.log(page);
            },
            pageSize: 3
          }}
          dataSource={data}
          // footer={
          //   <div>
          //     <b>ant design</b> footer part
          //   </div>
          // }
          renderItem={item => <GameItemList id={item.id} />}
        />
      </Spin>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    data: state.data,
    seacrh: state.searchText
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchData, searchFilter }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(GameList);
