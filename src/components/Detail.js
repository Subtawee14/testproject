import React, { useEffect, useState } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { SelectedMenu } from "../actions/MenuAction";
import { Row, Col, Icon, Button } from "antd";
import { fetchData } from "../reducer/fetchReducer";
import { useHistory } from "react-router-dom";
//import { detail } from '../data'
const Detail = props => {
  const [detail, setDetail] = useState();
  const { data } = props.data;
  const { fetchData } = props;
  let history = useHistory();

  useEffect(() => {
    if (data.length == 0) {
      fetchData();
    }

  }, []);

  useEffect(() => {
    fetchDataFilter();
  }, [data]);

  const fetchDataFilter = () => {
    console.log(data);
    const filter = data.find(value => {
      return value.id == props.match.params.id;
    });

    setDetail(filter);
  };
  console.log(detail);
  const renderDetail = () => {
    if (detail) {
      return (
        <Row>
          <Row>
            <Col span={7}>
              <h1>Name : {detail.data.name}</h1>
              <p>ID : {detail.data.steam_appid}</p>
              <p>Type : {detail.data.type}</p>
              <p>Required_age : {detail.data.required_age}</p>
            </Col>
            <Col span={17}>
              <h1>Description</h1>
              <div
                dangerouslySetInnerHTML={{
                  __html: detail.data.detailed_description
                }}
              ></div>
            </Col>
          </Row>
          <Row style={{ marginTop: "35px" }}>
            <Col span={7}>
              <img src={detail.data.header_image} />
            </Col>
            <Col span={17}>
              <h1>About The Game</h1>
              <div
                dangerouslySetInnerHTML={{ __html: detail.data.about_the_game }}
              ></div>
            </Col>
          </Row>
        </Row>
      );
    } else {
      return <div></div>;
    }
  };
  return (
    <div>
      <Row style={{ textAlign: "right" }}>
        <a
          onClick={() => {
            history.push("/edit/"+props.match.params.id);
          }}
        >
          <Icon type="edit" style={{ fontSize: "30px", color: "#08c" }} />
        </a>
      </Row>
      {renderDetail()}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    data: state.data
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchData }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
